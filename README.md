% BookletImposer / pdfimposer
% Achieve some basic imposition on PDF documents


What is it ?
============

Bookletimposer is an utility to achieve some basic imposition on PDF
documents, especially designed to work on booklets.

Bookletimposer is implemented as a commandline and GTK+ interface to pdfimposer,
a reusable python module built on top of pyPdf.

It was tested on GNU/Linux althought it may work on any systems with a Python
interpreter.

Bookletimposer and pdfimposer are both free software released under the GNU
General Public License, either version 3 or (at your option) any later version.
See COPYING for the full text of the license.


Features
========

- transform linear documents to booklets
- transform booklets to linear documents
- reduce a document to put many on one sheet


Development state
=================

BookletImposer should be quite stable. Thanks to report bugs on
kjo@a4nancy.net.eu.org.

Dependencies
============

pdfimposer requires:

- python3 (>= 3.5)
- pyPdf2 (>= 1.26)

BookletImposer also requires:

- PyGObject
- gtk+ (>= 3.22)
- glib

In addition, the build and installation process requires:

- python3-distutils-extra
- pandoc

And to build the website:

- python3-epydoc
- xsltproc
- docbook-xsl

Quick installation
==================

Under debian, install the following packages:

    python3-pypdf2 python3-distutils-extra python3-gi gir1.2-gtk-3.0 gir1.2-glib-2.0 pandoc

Once the tarball downloaded and extracted:

    $ ./setup.py build

Then as root:

    # ./setup.py install


pdfimposer API documentation
============================

See generated epydoc documentation (available at
<https://kjo.herbesfolles.org/bookletimposer/api/>)


BookletImposer usage
====================

BookletImposer can be launched from the Application view,
or with te command:

    $ bookletimposer

Help on command line options is available in the man page.
