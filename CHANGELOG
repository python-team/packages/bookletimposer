Changelog
=========

0.3.1
-----

### bookletimposer

- fix debug function and variable name clash
- update hungarian translation

### wwebsite

- add title to pages
- update screenshot
- restore reference to the archive
- help: fix symlink

0.3
---

- update documentation and website
- update file headears
- add a Keywords entry to desktop file

### pdfimposer

- allow to pass a tuple or a list to set_layout
- update to PyPDF2
- support north american paper size
- add an option to support long-edge flip printing
- update to python3

### bookletimposer

- gui: select layout as a pair of spinbuttons
- allow to start without gtk
- only use local data if DEBUG environnment variable is set
- gui: add an option to support long-edge flip printing
- update german translation
- update french translation
- add danish translation
- add hungarian translation
- gui: fix "copy pages" button
- gui: update to GTK 3.22
- gui: fix icon names
- pass pylint checks

### tests

- add CLI integration tests

0.2
---

### pdfimposer

- fix crash when using numerous pages per sheet with few input pages
- fix unexistant variable in StreamConverter.__get_sequence_for_linearize
- fix baseclass call in AbstractConverter.get_output_format
- document private methode and cleanup code comments

### bookletimposer

- use gtk+3, glib and pygi instead of pygtk
- some Gnome HIG compliance:
    - use big buttons with icons for conversion type
    - remove close button
    - embed a progress bar instead of displaying a progress dialog
- make <Esc> and <Ctrl>q close the application
- don't try to import deprecated psyco
- use convert button instead of apply; this fixes the "same menmonics" bug
- help: simplify legal notice
- man: convert to markdown converted by pandoc
- fix calling unexistant method in ConverterPreferences.create_converter

0.1
---

- initial release
